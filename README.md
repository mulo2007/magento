# Coding exercise - Order Api
Author: Luis Ignacio Garcia Guruchaga
Date december 2018
## DESCRIPTION:
Application to receive orders in json format, validate and store them.

The application use a Google Cloud Firebase function to send emails for
the successfull transactions. This function has been developed with javascript

A job is executed with certain frecuency to check if there are pending
notifications to be sent, and send them. The frecuency can be configurable
in the properties of the application

### CONFIGURATION:
The configuration is done in the file: resources/application.properties changing the properties texts
    - To configure the email destinatary, change the property email.to
    - To configure the interval, change the property fixedDelay.in.milliseconds

#### RUN:
To run the application: mvn spring-boot::run
To run the application with test coverage : mvn test spring-boot::run

