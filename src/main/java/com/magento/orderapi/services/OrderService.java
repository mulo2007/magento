package com.magento.orderapi.services;

import com.magento.orderapi.domain.OrderBean;
import com.magento.orderapi.domain.OrderDetailDao;
import com.magento.orderapi.exceptions.OrderException;

/**
 * Service to storage a transaction information
 * @author  Luis Ignacio Garcia Guruchaga
 * @version 1.0
 * @date 18/12/2018
 */
@FunctionalInterface
public interface OrderService {

    /*
     * Service method to handle the insertions between the
     * controler and the data acces layer
     * @param orderBean OrderBean
     * @throws OrderException
     */
    OrderDetailDao create(OrderBean order) throws OrderException;
}
