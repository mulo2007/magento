package com.magento.orderapi.services;

import com.magento.orderapi.domain.OrderBean;

/**
 * Service to handle message operations
 * @author  Luis Ignacio Garcia Guruchaga
 * @version 1.0
 * @date 18/12/2018
 */
@FunctionalInterface
public interface MessageService {

    /*
     * Service method create a new json message
     * @param orderBean OrderBean
     */
    String createMessage(OrderBean orderBean);
}
