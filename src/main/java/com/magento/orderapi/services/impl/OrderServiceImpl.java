package com.magento.orderapi.services.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magento.orderapi.domain.LineDao;
import com.magento.orderapi.domain.LineBean;
import com.magento.orderapi.domain.OrderBean;
import com.magento.orderapi.domain.OrderDetailDao;
import com.magento.orderapi.exceptions.OrderException;
import com.magento.orderapi.repository.LineRepository;
import com.magento.orderapi.repository.OrderRepository;
import com.magento.orderapi.services.FireBaseService;
import com.magento.orderapi.services.MessageService;
import com.magento.orderapi.services.OrderService;
import com.magento.orderapi.services.OrderValidatorService;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * {@inheritDoc}
 */
@AllArgsConstructor
@Slf4j
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private LineRepository lineRepository;
    @Autowired
    private OrderValidatorService orderValidator;
    @Autowired
    private FireBaseService fireBaseService;
    @Autowired
    private MessageService messageService;

    /**
     * {@inheritDoc}
     */
    @Override public OrderDetailDao create(OrderBean orderBean) throws OrderException {
        if (!orderValidator.validateOrder(orderBean)) {
            throw new OrderException(1, "Sequence line numbers invalid");
        }
        OrderDetailDao orderDetailDao = orderRepository.findByIdAndStoreId(orderBean.getId(), orderBean.getStore_id());
        if (orderDetailDao != null){
            throw new OrderException(2, "Order already exist");
        }
        OrderDetailDao order = insertOrder(orderBean);
        fireBaseService.sendMessage(messageService.createMessage(orderBean));
        log.info("Order inserted sucessfully");
        return order;
    }

    private OrderDetailDao insertOrder(OrderBean orderBean) {
        List<LineDao> orderItems = orderBean.getLines().stream().map(l -> getLineRow(l)).collect(Collectors.toList());
        OrderDetailDao order = new OrderDetailDao(orderBean.getId(), orderBean.getStore_id(), orderItems);
        orderRepository.save(order);
        orderItems.stream().forEach(l -> l.setOrderId(order));
        lineRepository.saveAll(orderItems);
        return order;
    }

    private LineDao getLineRow(LineBean lineBean){
        return new LineDao(lineBean.getLineNumber(), lineBean.getSku());
    }

}
