package com.magento.orderapi.services.impl;

import java.util.stream.IntStream;

import org.springframework.stereotype.Service;

import com.magento.orderapi.domain.OrderBean;
import com.magento.orderapi.exceptions.OrderException;
import com.magento.orderapi.services.OrderValidatorService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class OrderValidatorServiceImpl implements OrderValidatorService {

    @Override
    public boolean validateOrder(OrderBean orderBean) throws OrderException {
        return IntStream.rangeClosed(1, orderBean.getLines().size()).allMatch(n -> orderBean.getLines().get(n-1).getLineNumber() == n );
    }
}
