package com.magento.orderapi.services.impl;

import com.magento.orderapi.domain.OrderBean;
import com.magento.orderapi.services.MessageService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * {@inheritDoc}
 */
@AllArgsConstructor
@Slf4j
@Service
public class MessageServiceImpl implements MessageService {
    private static final String message = "Order number %s to store %s products has been proccesed sucessfully, ";
    @Autowired
    private Environment env;

    /**
     * {@inheritDoc}
     */
    @Override
    public String createMessage(OrderBean orderBean) {
        String formatedMessage = String.format(message, orderBean.getId(), orderBean.getLines().size());
        JSONObject obj = new JSONObject();
        obj.put("to", env.getProperty("email.to"));
        obj.put("from", "ignaciogarciag@gmail.com");
        obj.put("subject", "Order Transaction Report");
        obj.put("content", formatedMessage);
        return obj.toString();
    }
}
