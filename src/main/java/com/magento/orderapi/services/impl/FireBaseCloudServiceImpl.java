package com.magento.orderapi.services.impl;

import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import javax.annotation.PostConstruct;
import javax.ws.rs.core.MediaType;
import org.apache.wink.client.ClientConfig;
import org.apache.wink.client.Resource;
import org.apache.wink.client.RestClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.magento.orderapi.domain.PendingMessageDao;
import com.magento.orderapi.repository.PendingMessagesRepository;
import com.magento.orderapi.services.FireBaseService;
import lombok.AllArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * {@inheritDoc}
 */
@AllArgsConstructor
@Setter
@Slf4j
@Service
public class FireBaseCloudServiceImpl implements FireBaseService {
    @Autowired
    private PendingMessagesRepository pendingMessagesRepository;
    private  HttpURLConnection con;
    private  URI endopoint;
    private  RestClient client;
    private  Resource resource;


    @PostConstruct
    public void init(){
        try {
            endopoint = new URI("https://us-central1-stoked-coder-157705.cloudfunctions.net/sendEmail");
            ClientConfig clientConfig = new ClientConfig();
            clientConfig.connectTimeout(30000);
            clientConfig.readTimeout(30000);
            client = new RestClient(clientConfig);
            resource = client.resource(endopoint);
        } catch (URISyntaxException e) {
            log.error("Error generating client URI");
        }
    }

    /**
     * {@inheritDoc}
     */
    public void sendMessage(String json){
        try {
            String response = (String) resource.contentType(MediaType.APPLICATION_JSON).post(String.class, json);
        } catch (Exception e) {
            log.error("Error sending notification for orders");
            pendingMessagesRepository.save(new PendingMessageDao(json, LocalDateTime.now()));
        }
    }
    public FireBaseCloudServiceImpl(){
        pendingMessagesRepository = null;
        con = null;;
        endopoint = null;;
        client = null;;
        resource = null;;
    }
}
