package com.magento.orderapi.services;

import com.magento.orderapi.domain.OrderBean;
import com.magento.orderapi.exceptions.OrderException;

public interface OrderValidatorService {

    public boolean validateOrder(OrderBean orderBean) throws OrderException;
}
