package com.magento.orderapi.services;

/**
 * Service to handle Google FireBase cloud operations
 * @author  Luis Ignacio Garcia Guruchaga
 * @version 1.0
 * @date 18/12/2018
 */
@FunctionalInterface
public interface FireBaseService {

    /*
     * Service method to send an email throught FireBase function
     * @param String json message to send
     * @throws OrderException
     */
    void sendMessage(String json);
}
