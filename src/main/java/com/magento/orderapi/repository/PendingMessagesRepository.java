package com.magento.orderapi.repository;

import org.springframework.data.repository.CrudRepository;

import com.magento.orderapi.domain.LineDao;
import com.magento.orderapi.domain.PendingMessageDao;

/**
 * This interface allow us to store and retrieve attempts
 */
public interface PendingMessagesRepository
        extends CrudRepository<PendingMessageDao, Long> {

}
