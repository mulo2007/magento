package com.magento.orderapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.magento.orderapi.domain.OrderDetailDao;

/**
 * This interface allows us to save and retrieve Orders
 */
public interface OrderRepository extends CrudRepository<OrderDetailDao, Long> {
    OrderDetailDao findByIdAndStoreId(long id, long storeId);
}
