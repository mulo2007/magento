package com.magento.orderapi.repository;

import org.springframework.data.repository.CrudRepository;

import com.magento.orderapi.domain.LineDao;

/**
 * This interface allow us to store and retrieve attempts
 */
public interface LineRepository extends CrudRepository<LineDao, Long> {

}
