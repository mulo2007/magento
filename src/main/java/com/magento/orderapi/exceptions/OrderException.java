package com.magento.orderapi.exceptions;

public class OrderException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -122881203965199183L;
	private int code = 201;

	public OrderException(String message, Exception cause) {
		super(message, cause);
	}

	public OrderException(String message, Throwable cause) {
		super(message, cause);
	}

	public OrderException(int code, String message) {
		super(message);
		this.code = code;
	}

	public OrderException(Throwable cause) {
		super(cause);
	}

	public int getCode() {
		return code;
	}
}