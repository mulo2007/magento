package com.magento.orderapi.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.magento.orderapi.domain.OrderDetailDao;
import com.magento.orderapi.domain.OrderRequestBean;
import com.magento.orderapi.exceptions.OrderException;
import com.magento.orderapi.services.OrderService;

import lombok.extern.slf4j.Slf4j;

/**
 * Controller to process orders transactions
 * @author  Luis Ignacio Garcia Guruchaga
 * @version 1.0
 * @date 18/12/2018
 */
@Slf4j
@RestController
@RequestMapping("/order")
final class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<OrderDetailDao> postResult(@RequestBody OrderRequestBean order) {
        ResponseEntity responseEntity;
        try {
            responseEntity = ResponseEntity.ok(orderService.create(order.getOrder()));
        } catch (OrderException e) {
            log.error(e.getMessage(), e);
            responseEntity = new ResponseEntity<>(e.getMessage(),  HttpStatus.UNPROCESSABLE_ENTITY);
        }
        return responseEntity;
    }
}
