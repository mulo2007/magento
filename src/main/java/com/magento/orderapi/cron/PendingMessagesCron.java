package com.magento.orderapi.cron;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import com.magento.orderapi.domain.PendingMessageDao;
import com.magento.orderapi.repository.PendingMessagesRepository;
import com.magento.orderapi.services.impl.FireBaseCloudServiceImpl;

/**
 * Cron expression to schedule a job to check the pending
 * emails to be sent, get and send them
 * @author  Luis Ignacio Garcia Guruchaga
 * @version 1.0
 * @date 18/12/2018
 */
@Configuration
@EnableScheduling
public class PendingMessagesCron {
    @Autowired
    PendingMessagesRepository pendingMessagesRepository;
    @Autowired
    FireBaseCloudServiceImpl fireBaseCloudServiceImpl;

    @Scheduled(fixedDelayString = "${fixedDelay.in.milliseconds}")
    public void scheduleFixedDelayTask() {
        Iterable<PendingMessageDao> pendingMessage = pendingMessagesRepository.findAll();
        pendingMessage.forEach(this::resolveOldMessage);
    }
    private void resolveOldMessage(PendingMessageDao message){
        fireBaseCloudServiceImpl.sendMessage(message.getMessage());
        pendingMessagesRepository.delete(message);
    }
}