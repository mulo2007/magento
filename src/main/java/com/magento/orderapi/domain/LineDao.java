package com.magento.orderapi.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@RequiredArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
@Entity
public class LineDao {

    @Id
    @GeneratedValue
    @Column(name="lineid")
    private long lineid;

    @Column(name="linenumber")
    private final long lineNumber;

    @Column(name="sku")
    private final String sku;

    @ManyToOne
    @JoinColumn(name = "id")
    private OrderDetailDao orderId;

    // Empty constructor for JSON/JPA
    LineDao() {
        lineNumber = 0;
        sku = null;
    }

    public void setOrderId(OrderDetailDao orderId) {
        this.orderId = orderId;
    }

    LineDao(final int id, final int lineNumber, final String sku) {
        this.lineid = id;
        this.lineNumber = lineNumber;
        this.sku = sku;
    }
}
