package com.magento.orderapi.domain;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
@Entity
public class PendingMessageDao {

    @Id
    @GeneratedValue
    @Column(name="id")
    private long id;

    @Column(name="message")
    private final String message;

    @Column(name="emaildate")
    private LocalDateTime emaildate;


    public PendingMessageDao(final String message, final LocalDateTime emaildate) {
        this.message = message;
        this.emaildate = emaildate;
    }

    public String getMessage() {
        return message;
    }


    // Empty constructor for JSON/JPA
    PendingMessageDao() {
        message = null;
    }
}
