package com.magento.orderapi.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

/**
 * Identifies the lines of the order
 */
//@AllArgsConstructor
@RequiredArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
public class LineBean {

    @JsonProperty("line_number")
    private final long lineNumber;

    @JsonProperty("sku")
    private final String sku;

    // Empty constructor for JSON/JPA
    LineBean() {
        lineNumber = -1;
        sku = null;
    }
}
