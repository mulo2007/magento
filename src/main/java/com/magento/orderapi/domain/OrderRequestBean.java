package com.magento.orderapi.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

/**
 * Identifies the order that is received
 */
@RequiredArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
public class OrderRequestBean {
    private OrderBean order;
}
