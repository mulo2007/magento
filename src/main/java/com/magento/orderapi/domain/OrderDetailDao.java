package com.magento.orderapi.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@RequiredArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
@Entity
public class OrderDetailDao {

    @Id
    private long id;

    @Column(name="storeId")
    private final long storeId;

    // Empty constructor for JSON/JPA
    OrderDetailDao() {
        storeId = -1;
    }
    public OrderDetailDao(long id, long storeId, List<LineDao> orderItems) {
        this.id = id;
        this.storeId = storeId;
    }
}
