package com.magento.orderapi.domain;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

/**
 * Identifies the order that is received
 */
@AllArgsConstructor
@RequiredArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
public final class OrderBean {

    private final long id;
    private final long store_id;
    private List<LineBean> lines;

    // Empty constructor for JSON/JPA
    OrderBean() {
        id = -1;
        store_id = -1;
        lines = null;
    }
}
