package com.magento.orderapi.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.magento.orderapi.domain.LineBean;
import com.magento.orderapi.domain.OrderBean;
import com.magento.orderapi.services.impl.MessageServiceImpl;
import com.magento.orderapi.services.impl.OrderServiceImpl;

import org.springframework.core.env.Environment;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MessageServiceTest {

    private MessageServiceImpl messageService;
    @Mock
    private Environment environment;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        given(environment.getProperty("email.to")).willReturn("ignaciogarciag@gmail.com");
        messageService = new MessageServiceImpl(environment);
    }

    @Test
    public void createMessageTest() {

        assertThat(messageService.createMessage(new OrderBean(1,1,
                Arrays.asList(new LineBean(1, "line1"),
                        new LineBean(2, "line2")))))
                .isEqualTo(
                    "{\"subject\":\"Order Transaction Report\",\"from\":\"ignaciogarciag@gmail.com\",\"to\":\"ignaciogarciag@gmail.com\",\"content\":\"Order number 1 for store 1 proucts has been proccesed sucessfully, \"}"
        );
    }

}

