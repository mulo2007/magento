package com.magento.orderapi.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;

import javax.ws.rs.core.MediaType;

import org.apache.wink.client.Resource;
import org.apache.wink.client.RestClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.magento.orderapi.domain.LineBean;
import com.magento.orderapi.domain.OrderBean;
import com.magento.orderapi.exceptions.OrderException;
import com.magento.orderapi.repository.PendingMessagesRepository;
import com.magento.orderapi.services.impl.FireBaseCloudServiceImpl;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FireBaseCloudServiceImplTest {

    private FireBaseCloudServiceImpl fireBaseCloudService;

    @Before
    public void setUp(){
        fireBaseCloudService = new FireBaseCloudServiceImpl();
        fireBaseCloudService.init();
    }

    @Test
    public void sendEmailTest() {
        String json = "{\n"
                + "  \"order\": {\n"
                + "    \"id\": 1,\n"
                + "    \"store_id\": 20,\n"
                + "    \"lines\": [\n"
                + "      {\n"
                + "        \"line_number\": 1,\n"
                + "        \"sku\": \"blue_sock\"\n"
                + "      },\n"
                + "      {\n"
                + "        \"line_number\": 2,\n"
                + "        \"sku\": \"red_sock\"\n"
                + "      }\n"
                + "    ]\n"
                + "  }\n"
                + "}";
        fireBaseCloudService.sendMessage(json);
    }
}

