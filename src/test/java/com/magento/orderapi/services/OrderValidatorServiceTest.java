package com.magento.orderapi.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.junit4.SpringRunner;

import com.magento.orderapi.domain.LineBean;
import com.magento.orderapi.domain.OrderBean;
import com.magento.orderapi.exceptions.OrderException;
import com.magento.orderapi.services.impl.MessageServiceImpl;
import com.magento.orderapi.services.impl.OrderValidatorServiceImpl;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderValidatorServiceTest {

    private OrderValidatorServiceImpl orderValidatorServiceImpl;
    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        orderValidatorServiceImpl = new OrderValidatorServiceImpl();
    }

    @Test
    public void validateOrderTest() {
        try {
            assertThat(orderValidatorServiceImpl.validateOrder(new OrderBean(1,1,
                    Arrays.asList(new LineBean(1, "line1"),
                            new LineBean(2, "line2")))))
                    .isEqualTo(true);
        } catch (OrderException e) {
        }
    }

}

