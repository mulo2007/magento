package com.magento.orderapi.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import java.util.Arrays;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import com.magento.orderapi.domain.LineBean;
import com.magento.orderapi.domain.OrderBean;
import com.magento.orderapi.exceptions.OrderException;
import com.magento.orderapi.repository.LineRepository;
import com.magento.orderapi.repository.OrderRepository;
import com.magento.orderapi.services.impl.OrderServiceImpl;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderServiceTest {

    private OrderServiceImpl orderService;
    @Mock
    private OrderRepository orderRepository;
    @Mock
    private LineRepository lineRepository;
    @Mock
    private OrderValidatorService orderValidator;
    @Mock
    private FireBaseService fireBaseService;
    @Mock
    private MessageService messageService;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        orderService = new OrderServiceImpl(orderRepository, lineRepository, orderValidator, fireBaseService, messageService);
    }

    @Test
    public void createInvalidSequencialTest() {
        try {
            orderService.create(new OrderBean(1,1,
                    Arrays.asList(new LineBean(1, "line1"),
                            new LineBean(2, "line2"))));
        } catch (OrderException e) {
            log.error(e.getMessage(), e);
        }
    }

    @Test
    public void createInvalidIdMessageTest() {
        OrderBean orderBean = new OrderBean(1,1,
                Arrays.asList(new LineBean(1, "line1"),
                        new LineBean(2, "line2")));
        try {
          given(orderValidator.validateOrder(orderBean)).willReturn(true);
          given(orderRepository.existsById(orderBean.getId())).willReturn(true);
          orderService.create(orderBean);
        } catch (OrderException e) {
            log.error(e.getMessage(), e);
        }
    }

    @Test
    public void createValidMessageTest() {
        OrderBean orderBean = new OrderBean(1,1,
                Arrays.asList(new LineBean(1, "line1"),
                        new LineBean(2, "line2")));
        try {
            given(orderValidator.validateOrder(orderBean)).willReturn(true);
            given(orderRepository.existsById(orderBean.getId())).willReturn(false);
            assertThat(orderService.create(orderBean)).isNotNull();
            orderService.create(orderBean);
        } catch (OrderException e) {
            log.error(e.getMessage(), e);
        }
    }
}

